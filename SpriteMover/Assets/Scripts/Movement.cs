﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
	private Transform tf;
	public float speed;

    // Start is called before the first frame update
    void Start()
    {
        //Get the Transform component of the gameObject this component is attached to
		tf = gameObject.GetComponent<Transform>();
        //Set the speed at which the gameObject will move
		speed = 3.0f;
    }

	// Update is called once per frame
	void Update()
	{
        //If either shift key is being held down the gameObject will only move one unit in the pressed direction
		if (Input.GetKey("left shift") || Input.GetKey("right shift"))
		{
			if (Input.GetKeyDown("up") || Input.GetKeyDown("w"))
			{
				tf.Translate(Vector3.up);
			}

			if (Input.GetKeyDown("right") || Input.GetKeyDown("d"))
			{
				tf.Translate(Vector3.right);
			}

			if (Input.GetKeyDown("left") || Input.GetKeyDown("a"))
			{
				tf.Translate(Vector3.left);
			}

			if (Input.GetKeyDown("down") || Input.GetKeyDown("s"))
			{
				tf.Translate(Vector3.down);
			}
		}
        //Otherwise the gameObject will move in the pressed direction at a rate of speed(variable)\second
		else
		{
			if (Input.GetKey("up") || Input.GetKey("w"))
			{
				tf.Translate(Vector3.up * Time.deltaTime * speed);
			}

			if (Input.GetKey("right") || Input.GetKey("d"))
			{
				tf.Translate(Vector3.right * Time.deltaTime * speed);
			}

			if (Input.GetKey("left") || Input.GetKey("a"))
			{
				tf.Translate(Vector3.left * Time.deltaTime * speed);
			}

			if (Input.GetKey("down") || Input.GetKey("s"))
			{
				tf.Translate(Vector3.down * Time.deltaTime * speed);
			}

		}

	}
}
