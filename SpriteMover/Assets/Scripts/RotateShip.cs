﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateShip : MonoBehaviour
{
	private Transform tf;
	public float speed;

    // Start is called before the first frame update
    void Start()
    {
        //Get the Transform component of the gameObject this component is attached to
        tf = gameObject.GetComponent<Transform>();
        //Set the speed at which the gameObject will rotate
        speed = 45.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("e"))
		{
            //Rotate the gameObject to the left in a 2D space
			tf.Rotate(Vector3.forward * Time.deltaTime * speed);
		}

		if (Input.GetKey("r"))
		{
            //Rotate the gameObject to the right in a 2D space
			tf.Rotate(Vector3.back * Time.deltaTime * speed);
		}

	}
}
