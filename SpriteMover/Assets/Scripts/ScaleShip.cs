﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleShip : MonoBehaviour
{
	private Transform tf;
	public float speed;

    // Start is called before the first frame update
    void Start()
    {
        //Get the Transform component of the gameObject this component is attached to
        tf = gameObject.GetComponent<Transform>();
        //Set the speed at which the gameObject will scale
        speed = 0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        
		if (Input.GetKey("m"))
		{
            //Scale the gameObject up
			tf.localScale += new Vector3(speed, speed, 0);
		}

		if (Input.GetKey("n"))
		{
            //Scale the gameObject down
			tf.localScale -= new Vector3(speed, speed, 0);
		}

	}
}
