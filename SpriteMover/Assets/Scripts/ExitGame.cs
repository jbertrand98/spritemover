﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //If the user presses the Escape key
        if (Input.GetKeyDown("escape"))
		{
            //Exit the application
			Application.Quit();
		}
    }
}
