﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour
{
    
	Transform tf;
	Quaternion qu;
    // Start is called before the first frame update
    void Start()
    {
        //Get the Transform component of the gameObject this component is attached to
		tf = gameObject.GetComponent<Transform>();
        //Set a Quaterion to the rotation value that the gameObject will return to
		qu.Set(0f, 0f, 0f, 1f);
	}

    // Update is called once per frame
    void Update()
    {
        //If the user presses the space bar
        if (Input.GetKeyDown("space"))
		{
            //Reset the position and rotation of the gameObjects transform to their base values
			tf.SetPositionAndRotation(Vector2.zero, qu);
            //Reset the scale of the gameObjects transform to its base value
			tf.localScale = new Vector3(1, 1, 1);
		}
    }
}
