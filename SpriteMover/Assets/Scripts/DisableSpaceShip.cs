﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableSpaceShip : MonoBehaviour
{

	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //If the user presses the Q key down
        if (Input.GetKeyDown("q"))
		{
            //Disable the gameIbject this component is connected to
			gameObject.SetActive(false);
		}
    }
}
