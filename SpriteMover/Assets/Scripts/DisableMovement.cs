﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableMovement : MonoBehaviour
{

	private Movement shipMovement;
    private RotateShip shipRotate;
    private ScaleShip shipScale;
    // Start is called before the first frame update
    void Start()
    {
        //Get the Movement component from the object this component is attached to
		shipMovement = gameObject.GetComponent<Movement>();
        //Get the RotateShip component from the object this component is attached to
        shipRotate = gameObject.GetComponent<RotateShip>();
        //Get the ScaleShip component from the object this component is attached to
        shipScale = gameObject.GetComponent<ScaleShip>();
    }

    // Update is called once per frame
    void Update()
    {
        //If the user presses the P key
        if (Input.GetKeyDown("p"))
		{
            //Reverse the enabled state of the Movement component
			shipMovement.enabled = !shipMovement.enabled;
            //Reverse the enabled state of the RotateShip component
            shipRotate.enabled = !shipRotate.enabled;
            //Reverse the enabled state of the ScaleShip component
            shipScale.enabled = !shipScale.enabled;
		}
    }
}
